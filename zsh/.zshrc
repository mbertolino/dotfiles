
# Modules
autoload -Uz compinit vcs_info
compinit

# vcs_info configurations
precmd_vcs_info() { vcs_info }
precmd_functions+=( precmd_vcs_info )
setopt prompt_subst
RPROMPT=\$vcs_info_msg_0_
zstyle ':vcs_info:git*' actionformats "%u%c %b"
zstyle ':vcs_info:*' check-for-changes true

zle -N edit-command-line
zle -N zle-line-init
zle -N zle-keymap-select

# Shell options
setopt auto_cd \
    glob_dots \
    hist_verify \
    hist_append \
    prompt_subst \
    share_history \
    extended_glob \
    rm_star_silent \
    hist_fcntl_lock \
    complete_aliases \
    numeric_glob_sort \
    hist_save_no_dups \
    hist_ignore_space \
    hist_ignore_space \
    hist_reduce_blanks \
    inc_append_history \
    hist_ignore_all_dups \

READNULLCMD=$PAGER
HELPDIR=/usr/share/zsh/$ZSH_VERSION/help
HISTFILE=~/.config/zsh/.zhistory
HISTSIZE=25000
SAVEHIST=$HISTSIZE

#
# Style
# 
zstyle ':completion:*' menu select
zstyle ':completion:*' use-cache on
zstyle ':completion:*' rehash yes
zstyle ':completion:*' list-colors
zstyle ':completion:*' special-dirs true

PROMPT='%m%F{green}${branch}%f %F{cyan}%~ %f'


#
# Behaviour
#
# vimode, insert mode by default, replace indicators
function zle-line-init zle-keymap-select {
    vimode=${${KEYMAP/vicmd/n}/(main|viins)/i}
    zle reset-prompt
}
# Delete word
backward-kill-dir () {
    local WORDCHARS=${WORDCHARS/\/}
    zle backward-kill-word
}
zle -N backward-kill-dir
bindkey '^[^?' backward-kill-dir

# Colorscheme
# Base16 Shell Default
BASE16_SHELL=$HOME/.config/base16-shell/
[ -n "$PS1" ] && [ -s $BASE16_SHELL/profile_helper.sh ] && eval "$($BASE16_SHELL/profile_helper.sh)"

#
# Aliases
#
# Misc
alias rm='rm -vI'
alias cp='cp -vi'
alias mv='mv -vi'
alias ls='ls --color=auto --group-directories-first'
alias ll='ls -lhrt --color=auto --group-directories-first'
alias la='ls -a --color=auto --group-directories-first'
alias lsa='ls -lahrt --color=auto --group-directories-first'
alias copy="xclip -sel clip"
alias vim='vim --servername VIM'
alias grep='grep --color=auto'
alias rename='rename -v'
alias lex='~/.scripts/sdcv_filtered.sh'
alias calc='ipython -ic "import numpy as np; from math import *; import cmath; from scipy import special; import pint; u=pint.UnitRegistry()"'
alias aur="~/.scripts/aur.sh"

# Internet
alias parch="ping archlinux.org -c 3"
alias internetup="sudo rfkill unblock all && sudo ip link set wlp2s0 up"
alias internetdown="sudo ip link set wlp2s0 down && sudo rfkill block all"
alias vpnup="sudo wg-quick up /etc/wireguard/mullvad-se2.conf"
alias vpndown="sudo wg-quick down /etc/wireguard/mullvad-se2.conf"
alias vpnrestart="vpndown && vpnup && parch"

# ssh related
alias sshwatto="ssh -i ~/.ssh/watto mbertolino@watto.matfys.lth.se"
alias scpwatto="~/.scripts/scpwatto.sh"
alias sshmelo="ssh -i ~/.ssh/melo mela@melo -p 49494"
alias sshpero="ssh -i ~/.ssh/pero pera@pero -p 49494"

# Work
alias tdcisdiag="cd tdcismodule && python2 run_diag.py && cd .."
alias getres="scp -i ~/.ssh/watto mbertolino@watto.matfys.lth.se:~/results.tar.gz ."
alias plot="gnuplot --persist -c *.gp"
alias replaceHe="rm -rf ../He && cp -r"
alias mvres="~/.scripts/mvres.sh"
alias nys="echo $(date) >> ~/nysning.txt"
