#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '
alias 5='cd && cd ~/Backup/Studier/Teknisk\ Fysik/5'
alias vim='vim --servername VIM'
