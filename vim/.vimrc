"""""""""""""""""""""""
""      .vimrc       ""
""     Bertolino     ""
"""""""""""""""""""""""

"
" Vundle required
"
set nocompatible              " be iMproved, required
filetype off                  " required
set rtp+=~/.vim/bundle/Vundle.vim " Runtime path and initialize

call vundle#begin()
Plugin 'gmarik/Vundle.vim'
Plugin 'altercation/vim-colors-solarized'
Plugin 'terryma/vim-expand-region'
Plugin 'scrooloose/nerdtree'
Plugin 'lervag/vimtex'
Plugin 'SirVer/ultisnips'
Plugin 'honza/vim-snippets'
Plugin 'JuliaEditorSupport/julia-vim'
call vundle#end()            " required
filetype plugin indent on    " required

"
" Brief help
"
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal


"
" General
"
set encoding=utf-8
set history=1000
set clipboard=unnamed		" Map default copy/paste
vnoremap <C-c> "*y
set backup
set backupdir=~/.vim/backup	" Backup directory
set directory=~/.vim/swap	" Swap directory
runtime macros/matchit.vim

"
" Appearance
"
syntax enable
set background=dark
"color desert                    " load a colorscheme
color nord                    " load a colorscheme
let g:Powerline_symbols = 'unicode'
set number relativenumber
hi LineNr ctermfg=White
set linespace=0                 " no extra spaces between rows
set showmatch                   " show matching brackets/parenthesis
set scrolljump=5                " lines to scroll when cursor leaves screen
set scrolloff=3                 " minimum lines to keep above and below cursor
set foldenable                  " auto fold code
set foldlevel=99                " folding level
set foldmethod=indent           " folding method
if &term =~ "xterm\\|rxvt"      " change cursor color on mode  
  :silent !echo -ne "\033]12;darkcyan\007"
  let &t_SI = "\033]12;lightgreen\007"
  let &t_EI = "\033]12;darkcyan\007"
  autocmd VimLeave * :!echo -ne "\033]12;grey\007"
endif
set spelllang=en,sv

"
" Filetype specific
"
" Mail
autocmd FileType mail setlocal spell        " set spell for email
"autocmd BufRead ~/.mutt/temp/neomutt-* execute "normal /^$/\n" 
autocmd BufRead ~/.mutt/temp/neomutt-* execute ":startinsert"
autocmd BufRead ~/.mutt/temp/neomutt-* execute "o"
" Python
autocmd FileType python nnoremap <buffer> <F9> :exec '!ipython' shellescape(@%, 1)<cr>

"
" UI
" 
"set laststatus=2	      " Installera powerline istället
set selectmode+=mouse   " add mouse for select
set mouse=a		          " Enable mouse
set autoread	        	" Reloads file if changed outside


"
" Searching
"
set incsearch           " Incremental searching
set ignorecase          " Case insensitive
set smartcase           " unless at least 1 capital letter


"
" Formatting
"
set autoindent          " indent
set smarttab            " make tab and backspace smarter
set shiftwidth=2        " a tab is two spaces
set tabstop=1           " indentation of two spaces
set expandtab           " use spaces, not tabs
set backspace=indent,eol,start  " backspace through everything in insert mode
let fortran_free_source=1
let fortran_have_tabs=1
let fortran_more_precise=1
let fortran_do_enddo=1

"
" Plugins
"
" Vimtex
let g:latex_view_general_viewer = 'zathura'
let g:vimtex_view_method = 'zathura'
let g:LatexBox_viewer= '/usr/bin/zathura --fork --synctex-editor-command "vim --servername VIM --remote +\%{line} \%{input}"'
let g:vimtex_complete_enabled = 1
let g:vimtex_complete_close_braces = 1
let g:vimtex_complete_recursive_bib = 1

" vim-airline
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#show_splits = 0
let g:airline#extensions#tabline#formatter = 'jsformatter'
let g:airline_powerline_fonts = 1
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.whitespace = 'Ξ'


" vim-expand-region
vmap v <Plug>(expand_region_expand)
vmap <C-v> <Plug>(expand_region_shrink)

" UltiSnips
set runtimepath+=~/.vim/UltiSnips/
set runtimepath+=~/.vim/bundle/vim-snippets/
let g:UltiSnipsEditSplit='vertical'         " Use vertical split to edit snippets
let g:UltiSnipsExpandTrigger='<TAB>'
let g:UltiSnipsJumpForwardTrigger='<C-j>'   " Go to next snippet with Ctrl-j
let g:UltiSnipsJumpBackwardTrigger='<C-k>'  " Go to previous snippet with Ctrl-k

" Julia-vim
let g:latex_to_unicode_auto = 1

" NERDtree
map <C-n> :NERDTreeToggle<CR>
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
