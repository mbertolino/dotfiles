#!/bin/sh
notmuch new
# retag all "new" messages "inbox" and "unread"
notmuch tag +inbox +unread -new -- tag:new
# tag all messages from "me" as sent and remove tags inbox and unread
notmuch tag -inbox -new +sent -- from:mattias.bertolino@matfys.lth.se
# tag newsletters, but dont show them in inbox
notmuch tag -inbox +newsletter +unread -new -- from:kommunikation@fysik.lu.se or from:aktuelLTH@kansli.lth.se or subject:'*Indoor football*' or subject:'Newsletter*' or subject:'Dekanbrev*'
